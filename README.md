##JFinal-commons
该项目最早是在[`媒体猫`(java saas)](http://www.meitimao.com)中的一个模块

逐渐扩展成一系列项目的基础依赖，更加方便新JFinal-web系统的开发！


##功能
>更加安全美观的JCaptchaRender验证码

>自动安装数据库DatabaseBindPlugin插件

>短路检验器ShortCircuitValidator

>SessionId过滤器SessionIdHandler

>静态文件目录StaticHandler

>各种utils等你来发现


##关于媒体猫
`媒体猫`是一个基于docker，采用JFinal搭建的容器管理平台。

用户只需要简单的点击就可以启动一个属于他自己的web服务。


##联系我
`如梦技术`QQ群：237587118

##鸣谢
感谢`@JFinal`

感谢`@michaely`


## 捐助共勉
<img src="http://soft.dreamlu.net/weixin-9.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/weixin-19.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/alipay.png" width = "200" alt="支付宝捐助" align=center />

<img src="http://soft.dreamlu.net/qq-9.jpg" width = "200" alt="QQ捐助" align=center />
<img src="http://soft.dreamlu.net/qq-19.jpg" width = "200" alt="QQ捐助" align=center />
